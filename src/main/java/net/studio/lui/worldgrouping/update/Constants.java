package net.studio.lui.worldgrouping.update;

final class Constants {
    public static final String GITLAB_PROJECT_ID = "34309785";
    public static final String API_RELEASES_URL =
            String.format(
                    "https://gitlab.com/api/v4/projects/%s/releases?order_by=released_at&sort=desc",
                    GITLAB_PROJECT_ID
            );

    public static final String REGEX_RELEASE_CANDIDATE = "(?i).*?-rc.*$"; // (?i) enables case-insensitivity
    public static final String GITLAB_RELEASE_JSON_OBJECT_TAG_NAME = "tag_name";

    private Constants() {
    }
}
