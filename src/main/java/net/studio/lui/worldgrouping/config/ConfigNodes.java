package net.studio.lui.worldgrouping.config;

public final class ConfigNodes {
    private ConfigNodes() {
    }

    //messages
    public static final String PREFIX = "messages.prefix";
    public static final String NO_PERMISSION = "messages.commands.no-permission";
    public static final String HELP_LIST = "messages.commands.help";
    public static final String RELOAD_MESSAGE = "messages.commands.reload";
    public static final String GLOBAL_ON_MESSAGE = "messages.commands.global.toggle.true";
    public static final String GLOBAL_OFF_MESSAGE = "messages.commands.global.toggle.false";
    public static final String GLOBAL_FORMAT = "messages.commands.global.format";
    public static final String LIST_HEADER = "messages.commands.list.header";
    public static final String LIST_ITEM = "messages.commands.list.item";
    public static final String SHARED_GROUP_LIST_HEADER = "messages.commands.shared.header";
    public static final String SHARED_GROUP_LIST_ITEM = "messages.commands.shared.item";
    public static final String GROUP_MANAGER_EXIST = "messages.commands.group-manager.exist";
    public static final String GROUP_MANAGER_NOT_EXIST = "messages.commands.group-manager.not-exist";
    public static final String GROUP_MANAGER_ADDED = "messages.commands.group-manager.added";
    public static final String GROUP_MANAGER_DELETED = "messages.commands.group-manager.deleted";
    public static final String GROUP_MANAGER_MODIFY_HELP = "messages.commands.group-manager.modify.help";
    public static final String GROUP_MANAGER_MODIFY_NOT_EXIST = "messages.commands.group-manager.modify.not-exist";
    public static final String GROUP_MANAGER_MODIFY_GROUP_PART = "messages.commands.group-manager.modify.group-part";
    public static final String GROUP_MANAGER_MODIFY_NOT_GROUP_PART = "messages.commands.group-manager.modify.not-group-part";
    public static final String GROUP_MANAGER_MODIFY_ADDED = "messages.commands.group-manager.modify.added";
    public static final String GROUP_MANAGER_MODIFY_REMOVED = "messages.commands.group-manager.modify.removed";

    //options
    public static final String CHAT_ENABLED = "options.chat.enabled";
    public static final String TAB_ENABLED = "options.tablist.enabled";
    public static final String UPDATES_ENABLED = "options.update-checker.enabled";
    public static final String GLOBAL_CHAT_USERS = "global-chat-users";

    // shared groups
    public static final String SHARED_GROUP_LIST = "shared";
}
