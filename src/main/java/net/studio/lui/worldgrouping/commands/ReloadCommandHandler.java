package net.studio.lui.worldgrouping.commands;

import net.studio.lui.worldgrouping.config.ConfigManager;
import net.studio.lui.worldgrouping.config.ConfigNodes;
import net.studio.lui.worldgrouping.config.ConfigPermissions;
import org.bukkit.command.CommandSender;

/**
 * Handles the command "reload".
 */
public class ReloadCommandHandler extends CommandAdminHandler {
    protected ReloadCommandHandler(ConfigManager configManager) {
        super(configManager, "reload", ConfigPermissions.ADMIN);
    }

    @Override
    protected boolean handleConsoleOrAdminCommand(CommandSender commandSender, String[] args) {
        configManager.reload();
        commandSender.sendMessage(configManager.getLanguageValue(ConfigNodes.RELOAD_MESSAGE));
        return true;
    }
}
