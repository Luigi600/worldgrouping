package net.studio.lui.worldgrouping.commands;

import net.studio.lui.worldgrouping.config.ConfigManager;
import net.studio.lui.worldgrouping.config.ConfigNodes;
import net.studio.lui.worldgrouping.config.ConfigPermissions;
import net.studio.lui.worldgrouping.manager.GlobalManager;
import org.bukkit.entity.Player;

import java.util.UUID;
import java.util.stream.Collectors;

/**
 * Handles the command "global".
 */
public class GlobalCommandHandler extends CommandPlayerHandler {
    protected final GlobalManager globalManager;

    protected final ConfigManager userConfigManager;

    protected GlobalCommandHandler(ConfigManager configManager, ConfigManager userConfigManager, GlobalManager globalManager) {
        super(configManager, "global", ConfigPermissions.GLOBAL_CHAT_TOGGLE);

        this.globalManager = globalManager;
        this.userConfigManager = userConfigManager;
    }

    @Override
    protected boolean handlePlayerCommand(Player player, String[] args) {
        boolean toggled = globalManager.toggle(player);
        if (toggled)
            player.sendMessage(configManager.getLanguageValue(ConfigNodes.GLOBAL_ON_MESSAGE));
        else
            player.sendMessage(configManager.getLanguageValue(ConfigNodes.GLOBAL_OFF_MESSAGE));

        userConfigManager.setValueAndSave(
                ConfigNodes.GLOBAL_CHAT_USERS,
                globalManager.getToggled().stream().map(UUID::toString).collect(Collectors.toList())
        );

        return true;
    }
}
