package net.studio.lui.worldgrouping.commands;

import net.studio.lui.worldgrouping.config.ConfigManager;
import net.studio.lui.worldgrouping.util.ChatUtil;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

/**
 * Handles the input from the console OR from an administrator.
 */
public abstract class CommandAdminHandler extends CommandHandler<CommandSender> {
    protected final int maxCommandArgs;

    protected CommandAdminHandler(ConfigManager configManager, String command) {
        this(configManager, command, Constants.DEFAULT_AMOUNT_OF_COMMAND_ARGS, Constants.DEFAULT_PERMISSION);
    }

    protected CommandAdminHandler(ConfigManager configManager, String command, int maxCommandArgs) {
        this(configManager, command, maxCommandArgs, Constants.DEFAULT_PERMISSION);
    }

    protected CommandAdminHandler(ConfigManager configManager, String command, String... permissions) {
        this(configManager, command, Constants.DEFAULT_AMOUNT_OF_COMMAND_ARGS, permissions);
    }

    protected CommandAdminHandler(ConfigManager configManager, String command, int maxCommandArgs, String... permissions) {
        super(configManager, command, permissions);

        this.maxCommandArgs = maxCommandArgs;
    }

    @Override
    public boolean handleCommand(CommandSender commandSender, String[] args) {
        boolean isPlayer = (commandSender instanceof Player);
        if (isPlayer) {
            Player player = (Player) commandSender;

            if (!this.hasPermissionForCommand(player)) {
                ChatUtil.printNoPermissionsTo(player, configManager);
                return true;
            }
        } else if (!(commandSender instanceof ConsoleCommandSender)) {
            ChatUtil.printNoPermissionsTo(commandSender, configManager);
            return false;
        }

        if (args.length < this.maxCommandArgs) {
            this.printHelp(commandSender);
            return true;
        }

        return this.handleConsoleOrAdminCommand(commandSender, args);
    }

    protected abstract boolean handleConsoleOrAdminCommand(CommandSender commandSender, String[] args);

    protected void printHelp(CommandSender commandSender) {
        ChatUtil.printHelpTo(commandSender, configManager);
    }
}
