package net.studio.lui.worldgrouping.commands;

import net.studio.lui.worldgrouping.cloaks.Cloak;
import net.studio.lui.worldgrouping.config.ConfigManager;
import net.studio.lui.worldgrouping.config.ConfigNodes;
import net.studio.lui.worldgrouping.config.ConfigPermissions;
import net.studio.lui.worldgrouping.manager.SharedGroupManager;
import net.studio.lui.worldgrouping.sharedgroups.SharedGroup;
import net.studio.lui.worldgrouping.util.SharedGroupUtil;
import net.studio.lui.worldgrouping.util.string.StringPlaceholder;
import net.studio.lui.worldgrouping.util.string.StringUtil;
import org.bukkit.command.CommandSender;

import java.util.Optional;

/**
 * Handles the command "delete".
 */
public class GroupDeletionCommandHandler extends CommandAdminHandler {
    private final static int AMOUNT_OF_COMMAND_ARGS = 2;

    private final ConfigManager userConfigManager;
    private final SharedGroupManager sharedGroupManager;
    private final Cloak cloak;

    public GroupDeletionCommandHandler(ConfigManager configManager, ConfigManager userConfigManager, SharedGroupManager sharedGroupManager, Cloak cloak) {
        super(configManager, "delete", AMOUNT_OF_COMMAND_ARGS, ConfigPermissions.GROUPING_MANAGER);

        this.userConfigManager = userConfigManager;
        this.sharedGroupManager = sharedGroupManager;
        this.cloak = cloak;
    }

    @Override
    protected boolean handleConsoleOrAdminCommand(CommandSender commandSender, String[] args) {
        final String groupName = args[1];
        Optional<SharedGroup> sharedGroup = sharedGroupManager.getSharedGroupFromName(groupName);
        if (sharedGroup.isEmpty()) {
            commandSender.sendMessage(
                    StringUtil.placeholder(
                            configManager.getLanguageValue(ConfigNodes.GROUP_MANAGER_NOT_EXIST),
                            new StringPlaceholder("%group%", groupName)
                    )
            );
            return true;
        }

        this.sharedGroupManager.removeGroup(groupName);
        this.userConfigManager.setValueAndSave(
                ConfigNodes.SHARED_GROUP_LIST,
                SharedGroupUtil.convertSharedGroupsToConfigSection(this.sharedGroupManager.getSharedGroups())
        );
        this.sharedGroupManager.setInitState(this.cloak);
        commandSender.sendMessage(
                StringUtil.placeholder(
                        configManager.getLanguageValue(ConfigNodes.GROUP_MANAGER_DELETED),
                        new StringPlaceholder("%group%", groupName)
                )
        );
        return true;
    }
}
