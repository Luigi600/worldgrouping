package net.studio.lui.worldgrouping.commands;

import net.studio.lui.worldgrouping.cloaks.Cloak;
import net.studio.lui.worldgrouping.config.ConfigManager;
import net.studio.lui.worldgrouping.config.ConfigNodes;
import net.studio.lui.worldgrouping.config.ConfigPermissions;
import net.studio.lui.worldgrouping.manager.SharedGroupManager;
import net.studio.lui.worldgrouping.sharedgroups.SharedGroup;
import net.studio.lui.worldgrouping.util.SharedGroupUtil;
import net.studio.lui.worldgrouping.util.string.StringPlaceholder;
import net.studio.lui.worldgrouping.util.string.StringUtil;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Optional;

/**
 * Handles the command "modify".
 */
public class GroupModifyCommandHandler extends CommandAdminHandler {
    private final static int AMOUNT_OF_COMMAND_ARGS = 4;

    private final ConfigManager userConfigManager;
    private final SharedGroupManager sharedGroupManager;
    private final Cloak cloak;

    public GroupModifyCommandHandler(ConfigManager configManager, ConfigManager userConfigManager, SharedGroupManager sharedGroupManager, Cloak cloak) {
        super(configManager, "modify", AMOUNT_OF_COMMAND_ARGS, ConfigPermissions.GROUPING_MANAGER);

        this.userConfigManager = userConfigManager;
        this.sharedGroupManager = sharedGroupManager;
        this.cloak = cloak;
    }

    @Override
    protected boolean handleConsoleOrAdminCommand(CommandSender commandSender, String[] args) {
        final String eventName = args[1];

        // check if group exists
        final String groupName = args[2];
        Optional<SharedGroup> sharedGroup = sharedGroupManager.getSharedGroupFromName(groupName);
        if (sharedGroup.isEmpty()) {
            commandSender.sendMessage(
                    StringUtil.placeholder(
                            configManager.getLanguageValue(ConfigNodes.GROUP_MANAGER_NOT_EXIST),
                            new StringPlaceholder("%group%", groupName)
                    )
            );
            return true;
        }

        // check if world exists
        final String worldName = args[3];
        final World world = Bukkit.getWorld(worldName);
        if (world == null) {
            commandSender.sendMessage(
                    StringUtil.placeholder(
                            configManager.getLanguageValue(ConfigNodes.GROUP_MANAGER_MODIFY_NOT_EXIST),
                            new StringPlaceholder("%world%", worldName)
                    )
            );
            return true;
        }

        if (eventName.equalsIgnoreCase("add")) {
            this.addWorldToGroup(commandSender, sharedGroup.get(), world);
        } else if (eventName.equalsIgnoreCase("remove")) {
            this.removeWorldFromGroup(commandSender, sharedGroup.get(), world);
        }

        return true;
    }

    @Override
    protected void printHelp(CommandSender commandSender) {
        commandSender.sendMessage(configManager.getLanguageList(ConfigNodes.GROUP_MANAGER_MODIFY_HELP));
    }

    private void addWorldToGroup(CommandSender commandSender, SharedGroup sharedGroup, World world) {
        // check if world already a part of another group
        Optional<SharedGroup> sharedGroupFromWorld = sharedGroupManager.getSharedGroupFromWorld(world);
        if (sharedGroupFromWorld.isPresent()) {
            commandSender.sendMessage(
                    StringUtil.placeholder(
                            configManager.getLanguageValue(ConfigNodes.GROUP_MANAGER_MODIFY_GROUP_PART),
                            new StringPlaceholder("%group%", sharedGroupFromWorld.get().getGroupName()),
                            new StringPlaceholder("%world%", world.getName())
                    )
            );
            return;
        }

        sharedGroup.addWorld(world);
        this.userConfigManager.setValueAndSave(
                ConfigNodes.SHARED_GROUP_LIST,
                SharedGroupUtil.convertSharedGroupsToConfigSection(this.sharedGroupManager.getSharedGroups())
        );

        if (configManager.getBoolean(ConfigNodes.TAB_ENABLED)) {
            // TODO joined & left messages?
            for (Player player : world.getPlayers()) {
                for (World newVisWorld : sharedGroup.getWorlds()) {
                    this.cloak.showWorldAndPlayer(player, newVisWorld);
                }
            }
        }

        commandSender.sendMessage(
                StringUtil.placeholder(
                        configManager.getLanguageValue(ConfigNodes.GROUP_MANAGER_MODIFY_ADDED),
                        new StringPlaceholder("%group%", sharedGroup.getGroupName()),
                        new StringPlaceholder("%world%", world.getName())
                )
        );
    }

    private void removeWorldFromGroup(CommandSender commandSender, SharedGroup sharedGroup, World world) {
        if (!sharedGroup.removeWorld(world)) {
            commandSender.sendMessage(
                    StringUtil.placeholder(
                            configManager.getLanguageValue(ConfigNodes.GROUP_MANAGER_MODIFY_NOT_GROUP_PART),
                            new StringPlaceholder("%group%", sharedGroup.getGroupName()),
                            new StringPlaceholder("%world%", world.getName())
                    )
            );
            return;
        }

        if (configManager.getBoolean(ConfigNodes.TAB_ENABLED)) {
            // TODO joined & left messages?
            for (Player player : world.getPlayers()) {
                for (World newVisWorld : sharedGroup.getWorlds()) {
                    this.cloak.hideWorldAndPlayer(player, newVisWorld);
                }
            }
        }

        this.userConfigManager.setValueAndSave(
                ConfigNodes.SHARED_GROUP_LIST,
                SharedGroupUtil.convertSharedGroupsToConfigSection(this.sharedGroupManager.getSharedGroups())
        );
        commandSender.sendMessage(
                StringUtil.placeholder(
                        configManager.getLanguageValue(ConfigNodes.GROUP_MANAGER_MODIFY_REMOVED),
                        new StringPlaceholder("%group%", sharedGroup.getGroupName()),
                        new StringPlaceholder("%world%", world.getName())
                )
        );
    }
}
