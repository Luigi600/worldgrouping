package net.studio.lui.worldgrouping.commands;

import net.studio.lui.worldgrouping.config.ConfigManager;
import net.studio.lui.worldgrouping.util.ChatUtil;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Handles the input from a player.
 */
public abstract class CommandPlayerHandler extends CommandHandler<Player> {
    protected final int maxCommandArgs;

    protected CommandPlayerHandler(ConfigManager configManager, String command) {
        this(configManager, command, Constants.DEFAULT_AMOUNT_OF_COMMAND_ARGS, Constants.DEFAULT_PERMISSION);
    }

    protected CommandPlayerHandler(ConfigManager configManager, String command, int maxCommandArgs) {
        this(configManager, command, maxCommandArgs, Constants.DEFAULT_PERMISSION);
    }

    protected CommandPlayerHandler(ConfigManager configManager, String command, String... permissions) {
        this(configManager, command, Constants.DEFAULT_AMOUNT_OF_COMMAND_ARGS, permissions);
    }

    protected CommandPlayerHandler(ConfigManager configManager, String command, int maxCommandArgs, String... permissions) {
        super(configManager, command, permissions);

        this.maxCommandArgs = maxCommandArgs;
    }

    @Override
    public boolean handleCommand(CommandSender commandSender, String[] args) {
        if (!(commandSender instanceof Player))
            return false;

        Player player = (Player) commandSender;
        if (!this.hasPermissionForCommand(player)) {
            ChatUtil.printNoPermissionsTo(player, configManager);
            return true;
        }

        if (args.length < this.maxCommandArgs) {
            this.printHelp(commandSender);
            return true;
        }

        return this.handlePlayerCommand(player, args);
    }

    protected abstract boolean handlePlayerCommand(Player player, String[] args);

    protected void printHelp(CommandSender commandSender) {
        ChatUtil.printHelpTo(commandSender, configManager);
    }
}
