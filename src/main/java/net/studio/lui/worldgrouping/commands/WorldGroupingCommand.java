package net.studio.lui.worldgrouping.commands;

import net.studio.lui.worldgrouping.cloaks.Cloak;
import net.studio.lui.worldgrouping.config.ConfigManager;
import net.studio.lui.worldgrouping.manager.ChatManager;
import net.studio.lui.worldgrouping.manager.GlobalManager;
import net.studio.lui.worldgrouping.manager.SharedGroupManager;
import net.studio.lui.worldgrouping.util.ChatUtil;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Handles the main command "wg".
 */
public class WorldGroupingCommand implements CommandExecutor {
    private final ConfigManager configManager;
    private final List<CommandHandler<?>> handlers = new ArrayList<>();

    public WorldGroupingCommand(ConfigManager configManager, ConfigManager userConfigManager, GlobalManager globalManager, SharedGroupManager sharedGroupManager, ChatManager chatManager, Cloak cloak) {
        this.configManager = configManager;

        this.handlers.add(new ReloadCommandHandler(configManager));
        this.handlers.add(new ListCommandHandler(configManager));
        this.handlers.add(new GlobalCommandHandler(configManager, userConfigManager, globalManager));
        this.handlers.add(new GroupsCommandHandler(configManager, sharedGroupManager));
        this.handlers.add(new GroupModifyCommandHandler(configManager, userConfigManager, sharedGroupManager, cloak));
        this.handlers.add(new GroupCreationCommandHandler(configManager, userConfigManager, sharedGroupManager));
        this.handlers.add(new GroupDeletionCommandHandler(configManager, userConfigManager, sharedGroupManager, cloak));
        this.handlers.add(new MessageCommandHandler(configManager, sharedGroupManager, chatManager));
    }

    @Override
    public boolean onCommand(@NotNull CommandSender commandSender, Command command, @NotNull String label, String[] args) {
        String commandName = command.getName();
        if (Arrays.stream(Constants.COMMAND_NAME_LISTENERS).noneMatch(l -> l.equalsIgnoreCase(commandName)))
            return false;

        if (args.length == 0) {
            ChatUtil.printHelpTo(commandSender, configManager);
            return true;
        }

        for (CommandHandler<?> handler : this.handlers) {
            if (handler.getCommand().equalsIgnoreCase(args[0]) && handler.handleCommand(commandSender, args))
                return true;
        }

        ChatUtil.printHelpTo(commandSender, configManager);
        return true;
    }
}
