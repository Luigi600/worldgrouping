package net.studio.lui.worldgrouping.cloaks;

import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

public class Cloak_1_18_R1 extends Cloak {
    public Cloak_1_18_R1(JavaPlugin plugin) {
        super(plugin);
    }

    @Override
    public void show(Player player, Player toShow) {
        player.showPlayer(this.getPlugin(), toShow);
    }

    @Override
    public void hide(Player player, Player toHide) {
        player.hidePlayer(this.getPlugin(), toHide);
    }
}
