package net.studio.lui.worldgrouping.manager;

import net.studio.lui.worldgrouping.config.ConfigManager;
import net.studio.lui.worldgrouping.config.ConfigNodes;
import net.studio.lui.worldgrouping.config.ConfigPermissions;
import net.studio.lui.worldgrouping.util.string.StringPlaceholder;
import net.studio.lui.worldgrouping.util.string.StringUtil;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.UUID;

public class GlobalManager {
    private final ConfigManager configManager;
    private final ArrayList<UUID> toggled;

    public ArrayList<UUID> getToggled() {
        return this.toggled;
    }

    public GlobalManager(ConfigManager configManager, ConfigManager userConfigManager) {
        this.configManager = configManager;
        this.toggled = new ArrayList<>();

        for (String uuid : userConfigManager.getStringList(ConfigNodes.GLOBAL_CHAT_USERS)) {
            UUID playerUUID = UUID.fromString(uuid);
            Player player = Bukkit.getPlayer(playerUUID);
            if (player != null && ConfigPermissions.hasPermission(player, ConfigPermissions.GLOBAL_CHAT_TOGGLE)) {
                this.toggled.add(playerUUID);
            }
        }
    }

    public void broadcast(Player player, String message) {
        World world = player.getWorld();
        String global = StringUtil.placeholder(this.configManager.getLanguageValue(ConfigNodes.GLOBAL_FORMAT),
                new StringPlaceholder("%player%", player.getName()),
                new StringPlaceholder("%message%", message),
                new StringPlaceholder("%world%", world.getName()));

        this.toggled.forEach(uuid -> {
            Player toPlayer = Bukkit.getPlayer(uuid);
            if (toPlayer == null || toPlayer.getWorld().equals(world))
                return;

            toPlayer.sendMessage(global);
        });

    }

    public boolean toggle(Player player) {
        UUID uuid = player.getUniqueId();
        if (this.toggled.contains(uuid)) {
            this.toggled.remove(uuid);
            return false;
        }

        this.toggled.add(uuid);
        return true;
    }

    public boolean hasGlobalChat(Player player) {
        UUID uuid = player.getUniqueId();
        return this.toggled.contains(uuid);
    }
}