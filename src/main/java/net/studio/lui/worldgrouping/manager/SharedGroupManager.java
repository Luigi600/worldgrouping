package net.studio.lui.worldgrouping.manager;

import net.studio.lui.worldgrouping.cloaks.Cloak;
import net.studio.lui.worldgrouping.config.ConfigManager;
import net.studio.lui.worldgrouping.config.ConfigNodes;
import net.studio.lui.worldgrouping.sharedgroups.SharedGroup;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class SharedGroupManager {
    private final ConfigManager userConfigManager;
    private final ArrayList<SharedGroup> sharedGroups = new ArrayList<>();

    public ArrayList<SharedGroup> getSharedGroups() {
        return sharedGroups;
    }

    public SharedGroupManager(ConfigManager userConfigManager) {
        this.userConfigManager = userConfigManager;

        for (String grpName : userConfigManager.getListOfKeys(ConfigNodes.SHARED_GROUP_LIST)) {
            this.sharedGroups.add(new SharedGroup(grpName, userConfigManager));
        }
    }

    public void setInitState(Cloak cloak) {
        List<World> allWorlds = Bukkit.getWorlds();

        // get all worlds that are NOT in a group
        Bukkit.getWorlds()
                .stream()
                .filter(w ->
                        this.getSharedGroupFromWorld(w).isEmpty()
                )
                .forEach(w -> {
                            // get all players that are NOT in the selected world
                            // and hide the world
                            Bukkit.getOnlinePlayers()
                                    .stream()
                                    .filter(p -> !p.getWorld().equals(w))
                                    .forEach(p -> cloak.hideWorldAndPlayer(p, w));

                            // get all players in the selected world
                            // and show the world and players
                            w.getPlayers().forEach(p -> cloak.showWorldAndPlayer(p, w));
                        }
                );

        // get all groups that share the chat and TabLists
        for (SharedGroup group : this.getSharedGroups()) {
            List<World> groupWorlds = group.getWorlds();
            // show all worlds and players inside the group
            groupWorlds.forEach(w -> w.getPlayers().forEach(p -> cloak.showWorldAndPlayer(p, w)));
            // hide all worlds that are not in this group
            allWorlds
                    .stream()
                    .filter(w -> !groupWorlds.contains(w))
                    .forEach(w ->
                            group.getAllPlayers().forEach(p -> cloak.hideWorldAndPlayer(p, w))
                    );
        }
    }

    public boolean isInSameSharedGroup(World oldWorld, World newWorld) {
        Optional<SharedGroup> worldGrpOfOldWord = this.getSharedGroupFromWorld(oldWorld);
        Optional<SharedGroup> worldGrpOfNewWord = this.getSharedGroupFromWorld(newWorld);

        if (worldGrpOfOldWord.isEmpty() || worldGrpOfNewWord.isEmpty())
            return false;

        return worldGrpOfOldWord.get().equals(worldGrpOfNewWord.get());
    }

    public List<Player> getAllPlayersFromSharedGroup(World world) {
        List<World> worlds = this.getAllSharedWorldsFromWorld(world);
        ArrayList<Player> result = new ArrayList<>();

        for (World singleSharedWorld : worlds)
            result.addAll(singleSharedWorld.getPlayers());

        return result;
    }

    public Optional<SharedGroup> getSharedGroupFromWorld(World world) {
        return this.sharedGroups.stream().filter(g -> g.getWorlds().contains(world)).findFirst();
    }

    public Optional<SharedGroup> getSharedGroupFromName(String groupName) {
        return this.sharedGroups.stream().filter(g -> g.getGroupName().equals(groupName)).findFirst();
    }

    public List<World> getAllSharedWorldsFromWorld(World world) {
        ArrayList<World> result = new ArrayList<>();

        Optional<SharedGroup> sharedGrp = this.getSharedGroupFromWorld(world);
        if (sharedGrp.isEmpty())
            result.add(world);
        else
            result.addAll(sharedGrp.get().getWorlds());

        return result;
    }

    public void addGroup(String groupName) {
        if (this.getSharedGroupFromName(groupName).isPresent()) return;

        this.sharedGroups.add(new SharedGroup(groupName, this.userConfigManager));
    }

    public void removeGroup(String groupName) {
        Optional<SharedGroup> sharedGroup = this.getSharedGroupFromName(groupName);
        if (sharedGroup.isEmpty()) return;

        this.sharedGroups.remove(sharedGroup.get());
    }
}
