package net.studio.lui.worldgrouping.util;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.chat.TranslatableComponent;
import net.studio.lui.worldgrouping.config.ConfigManager;
import net.studio.lui.worldgrouping.config.ConfigNodes;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public final class ChatUtil {
    private ChatUtil() {
    }

    public static void printHelpTo(CommandSender commandSender, ConfigManager configManager) {
        commandSender.sendMessage(configManager.getLanguageList(ConfigNodes.HELP_LIST));
    }

    public static void printNoPermissionsTo(CommandSender commandSender, ConfigManager configManager) {
        commandSender.sendMessage(
                configManager.getLanguageValue(ConfigNodes.NO_PERMISSION)
        );
    }

    public static TranslatableComponent getJoinedMessage(Player player) {
        return getJoinedLeftMessage(player, "multiplayer.player.joined", ChatColor.YELLOW, ChatColor.YELLOW);
    }

    public static TranslatableComponent getLeftMessage(Player player) {
        return getJoinedLeftMessage(player, "multiplayer.player.left", ChatColor.YELLOW, ChatColor.YELLOW);
    }

    private static TranslatableComponent getJoinedLeftMessage(Player player, String translateKey) {
        return getJoinedLeftMessage(player, translateKey, ChatColor.YELLOW, ChatColor.YELLOW);
    }

    private static TranslatableComponent getJoinedLeftMessage(Player player, String translateKey, ChatColor messageColor, ChatColor usernameColor) {
        TranslatableComponent message = new TranslatableComponent(translateKey);
        message.setColor(messageColor);

        TextComponent username = new TextComponent(player.getDisplayName());
        username.setColor(usernameColor);

        message.addWith(username);

        return message;
    }
}
