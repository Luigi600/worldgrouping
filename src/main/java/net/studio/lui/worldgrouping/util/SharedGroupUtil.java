package net.studio.lui.worldgrouping.util;

import net.studio.lui.worldgrouping.sharedgroups.SharedGroup;
import org.bukkit.generator.WorldInfo;

import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

public final class SharedGroupUtil {
    private SharedGroupUtil() {
    }

    public static HashMap<String, List<String>> convertSharedGroupsToConfigSection(List<SharedGroup> sharedGroups) {
        HashMap<String, List<String>> result = new HashMap<>();

        for (SharedGroup sharedGroup : sharedGroups)
            result.put(
                    sharedGroup.getGroupName(),
                    sharedGroup.getWorlds().stream().map(WorldInfo::getName).collect(Collectors.toList())
            );

        return result;
    }
}
