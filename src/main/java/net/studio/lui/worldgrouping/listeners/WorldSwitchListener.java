package net.studio.lui.worldgrouping.listeners;

import net.studio.lui.worldgrouping.config.ConfigManager;
import net.studio.lui.worldgrouping.config.ConfigNodes;
import net.studio.lui.worldgrouping.manager.SharedGroupManager;
import net.studio.lui.worldgrouping.cloaks.Cloak;
import net.studio.lui.worldgrouping.util.string.StringUtil;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerChangedWorldEvent;
import org.bukkit.event.server.ServerLoadEvent;

import java.util.ArrayList;
import java.util.List;

public class WorldSwitchListener implements Listener {
    private final boolean enabled;
    private final SharedGroupManager sharedGroupManager;
    private final Cloak cloak;
    private final List<PlayerChangeGroupListener> playerChangeGroupListeners = new ArrayList<>();

    public WorldSwitchListener(ConfigManager configManager, SharedGroupManager sharedGroupManager, Cloak cloak) {
        this.sharedGroupManager = sharedGroupManager;
        this.cloak = cloak;

        // again with the cool shorthand
        if (!(this.enabled = configManager.getBoolean(ConfigNodes.TAB_ENABLED))) {
            ConsoleCommandSender sender = Bukkit.getConsoleSender();
            sender.sendMessage(StringUtil.color("&r[WorldGrouping] &lNOTE:&r You have set per world tab to: &cdisabled&r."));
        }

    }

    public void addPlayerChangeGroupListener(PlayerChangeGroupListener listener) {
        this.playerChangeGroupListeners.add(listener);
    }

    @EventHandler
    public void onPluginLoad(ServerLoadEvent event) {
        if (!this.enabled)
            return;

        this.sharedGroupManager.setInitState(cloak);
    }

    @EventHandler
    public void onWorldChange(PlayerChangedWorldEvent event) {
        if (!this.enabled)
            return;

        Player player = event.getPlayer();
        World from = event.getFrom();
        World to = player.getWorld();

        if (sharedGroupManager.isInSameSharedGroup(from, to)) return;

        List<World> sharedWorldsFrom = sharedGroupManager.getAllSharedWorldsFromWorld(from);
        List<World> sharedWorldsTo = sharedGroupManager.getAllSharedWorldsFromWorld(to);

        sharedWorldsFrom.forEach(w -> this.cloak.hideWorldAndPlayer(player, w));
        playerChangeGroupListeners.forEach(l -> l.onLeftGroup(player, sharedWorldsFrom));
        sharedWorldsTo.forEach(w -> this.cloak.showWorldAndPlayer(player, w));
        playerChangeGroupListeners.forEach(l -> l.onJoinedGroup(player, sharedWorldsTo));
    }
}
