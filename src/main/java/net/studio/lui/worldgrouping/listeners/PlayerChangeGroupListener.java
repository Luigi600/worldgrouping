package net.studio.lui.worldgrouping.listeners;

import org.bukkit.World;
import org.bukkit.entity.Player;

import java.util.List;

public interface PlayerChangeGroupListener {
    void onLeftGroup(Player player, List<World> worlds);

    void onJoinedGroup(Player player, List<World> worlds);
}
