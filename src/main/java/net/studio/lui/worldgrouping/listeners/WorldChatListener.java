package net.studio.lui.worldgrouping.listeners;

import net.studio.lui.worldgrouping.config.ConfigManager;
import net.studio.lui.worldgrouping.config.ConfigNodes;
import net.studio.lui.worldgrouping.manager.ChatManager;
import net.studio.lui.worldgrouping.manager.GlobalManager;
import net.studio.lui.worldgrouping.manager.SharedGroupManager;
import net.studio.lui.worldgrouping.util.string.StringUtil;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import java.util.List;

public class WorldChatListener implements Listener {
    private final boolean enabled;
    private final GlobalManager globalManager;
    private final SharedGroupManager sharedGroupManager;
    private final ChatManager chatManager;

    public WorldChatListener(ConfigManager configManager, GlobalManager globalManager, SharedGroupManager sharedGroupManager, ChatManager chatManager) {
        this.globalManager = globalManager;
        this.sharedGroupManager = sharedGroupManager;
        this.chatManager = chatManager;

        // exercising some cool shorthand here
        if (!(this.enabled = configManager.getBoolean(ConfigNodes.CHAT_ENABLED))) {
            ConsoleCommandSender sender = Bukkit.getConsoleSender();
            sender.sendMessage(StringUtil.color("&r[WorldGrouping] &lNOTE:&r You have set per world chat to: &cdisabled&r."));
        }
    }

    @EventHandler
    public void onChat(AsyncPlayerChatEvent event) {
        /*
            This event can get problematic easily: it is async
            so most BukkitAPI interaction will be unavailable.
         */

        if (!this.enabled)
            return;

        if (this.handleAdminMessage(event))
            return;

        this.handleMessageVisibility(event);
    }

    private boolean handleAdminMessage(AsyncPlayerChatEvent event) {
        List<World> target = this.chatManager.getTargets(event.getPlayer(), event.getMessage());
        if (target == null) return false;

        this.chatManager.removeMessage(event.getPlayer(), event.getMessage());

        event.getRecipients().clear();
        event.getRecipients().add(event.getPlayer()); // show message in the chat of the message owner
        target.forEach(w -> event.getRecipients().addAll(w.getPlayers()));

        return true;
    }

    private void handleMessageVisibility(AsyncPlayerChatEvent event) {
        Player player = event.getPlayer();
        World world = player.getWorld();
        String message = event.getMessage();

        // modify the recipients, this preserves the chat format set by other plugins
        event.getRecipients().clear();
        event.getRecipients().addAll(this.sharedGroupManager.getAllPlayersFromSharedGroup(world)); //simple, heh

        globalManager.broadcast(player, message);
    }
}
